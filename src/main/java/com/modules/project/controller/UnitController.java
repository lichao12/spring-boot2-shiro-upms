package com.modules.project.controller;

import com.modules.common.enmus.PublicEnum;
import com.modules.common.poi.ExcelUtil;
import com.modules.common.web.BaseController;
import com.modules.common.web.Result;
import com.modules.project.entity.Unit;
import com.modules.project.service.UnitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 单位管理
 *
 * @author lc
 */
@Api(tags = "单位管理")
@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/project/unit")
public class UnitController extends BaseController {

    @Autowired
    private UnitService unitService;

    /**
     * 新增单位
     *
     * @param unit
     * @return
     */
    @ApiOperation(value = "新增单位", notes = "新增单位")
    @PostMapping
    public Result add(@Validated @RequestBody Unit unit){
        if (PublicEnum.NOT_UNIQUE.getCode().equals(unitService.checkUnitNoUnique(unit)))
        {
            return error("新增单位序号'" + unit.getUnitNo() + "'失败，该序号已存在");
        }
        return success(unitService.insertUnit(unit));
    }

    /**
     * 修改单位
     *
     * @param unit
     * @return
     */
    @ApiOperation(value = "修改单位", notes = "修改单位")
    @PutMapping
    public Result edit(@Validated @RequestBody Unit unit){
        if (PublicEnum.NOT_UNIQUE.getCode().equals(unitService.checkUnitNoUnique(unit)))
        {
            return error("修改单位序号'" + unit.getUnitNo() + "'失败，该序号已存在");
        }
        return success(unitService.updateUnit(unit));
    }

    /**
     * 删除单位
     */
    @ApiOperation(value="删除单位",notes = "删除单位")
    @DeleteMapping("/{unitId}")
    public Result remove(@PathVariable Long unitId)
    {
        return success(unitService.deleteUnitById(unitId));
    }

    /**
     * 根据ID获取详细信息
     */
    @ApiOperation(value="根据ID获取详细信息",notes = "根据ID获取详细信息")
    @GetMapping(value = "/{unitId}")
    public Result selectUnitById(@PathVariable(value = "unitId", required = true) Long unitId)
    {
        return success(unitService.selectUnitById(unitId));
    }

    /**
     * 获取单位树列表
     */
    @ApiOperation(value="获取单位树列表",notes = "获取单位树列表")
    @GetMapping(value = "/selectUnitTreeList")
    public Result selectUnitTreeList(@Validated Unit unit)
    {
        return success(unitService.selectUnitTreeList(unit));
    }

    /**
     * 通过EXCEL模板文件导入单位数据
     *
     */
    @ApiOperation(value="通过EXCEL模板文件导入单位数据",notes = "通过EXCEL模板文件导入单位数据")
    @PostMapping("/importData")
    @ResponseBody
    public Result importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Unit> util = new ExcelUtil<Unit>(Unit.class);
        List<Unit> list = util.importExcel(file.getInputStream());
        String message = unitService.importUnit(list, updateSupport);
        return success(message);
    }

}
