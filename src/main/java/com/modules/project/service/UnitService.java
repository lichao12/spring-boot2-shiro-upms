package com.modules.project.service;

import com.modules.common.enmus.PublicEnum;
import com.modules.common.utils.StringUtils;
import com.modules.project.dao.UnitMapper;
import com.modules.project.entity.Unit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 单位管理
 *
 * @author li'chao
 */
@Slf4j
@Service
public class UnitService {

    @Autowired
    private UnitMapper unitMapper;

    /**
     * 新增单位
     *
     * @param unit
     * @return
     */
    public int insertUnit(Unit unit){
        return unitMapper.insert(unit);
    }

    /**
     * 修改单位
     *
     * @param unit
     * @return
     */
    public int updateUnit(Unit unit){
        return unitMapper.updateByPrimaryKeySelective(unit);
    }

    /**
     * 根据ID删除单位
     *
     * @param unitId
     * @return
     */
    public int deleteUnitById(Long unitId){
        return unitMapper.deleteByPrimaryKey(unitId);
    }

    /**
     * 根据ID查询单位
     *
     * @param unitId
     * @return
     */
    public Unit selectUnitById(Long unitId){
        return unitMapper.selectByPrimaryKey(unitId);
    }

    /**
     * 校验单位序号是否唯一
     *
     * @param unit
     * @return
     */
    public String checkUnitNoUnique(Unit unit)
    {
        Long unidId = StringUtils.isNull(unit.getUnitId()) ? -1L : unit.getUnitId();
        Unit info = unitMapper.checkUnitNoUnique(unit.getUnitNo());
        if (StringUtils.isNotNull(info) && info.getUnitId().longValue() != unidId.longValue())
        {
            return PublicEnum.NOT_UNIQUE.getCode();
        }
        return PublicEnum.UNIQUE.getCode();
    }

    /**
     * 单位树列表
     * @return 单位树列表
     */
    public List<Unit> selectUnitTreeList(Unit unit){
        List<Unit> allMenu = unitMapper.selectUnitList(unit);
        //根节点
        List<Unit> rootMenu = new ArrayList<Unit>();
        allMenu.forEach(nav ->{
            if("0".equals(nav.getParentNo())){//父节点是0的，为根节点。
                rootMenu.add(nav);
            }
        });
        //为根单位设置子集，getClild是递归调用的
        rootMenu.forEach(nav ->{
            /* 获取根节点下的所有子节点 使用getChild方法*/
            List<Unit> childList = getChild(String.valueOf(nav.getUnitNo()), allMenu);
            nav.setChildren(childList);//给根节点设置子节点
        });
        /* 根据Menu类的order排序 */
        rootMenu.sort(Comparator.comparing(Unit::getParentNo));
        return rootMenu;
    }

    /**
     * 获取子节点
     * @param id 父节点id
     * @param allMenu 所有单位列表
     * @return 每个根节点下，所有子单位列表
     */
    public List<Unit> getChild(String id, List<Unit> allMenu){
        //子菜单
        List<Unit> childList = new ArrayList<Unit>();
        allMenu.forEach(nav ->{
            // 遍历所有节点，将所有单位的父id与传过来的根节点的id比较
            //相等说明：为该根节点的子节点。
            if(nav.getParentNo().equals(id)){
                childList.add(nav);
            }
        });
        //递归
        childList.forEach(nav ->{
            nav.setChildren(getChild(nav.getUnitNo(), allMenu));
        });
        childList.sort(Comparator.comparing(Unit::getParentNo));//排序
        //如果节点下没有子节点，返回一个空List（递归退出）
        if(childList.size() == 0){
            return new ArrayList<Unit>();
        }
        return childList;
    }

    /**
     * 导入单位数据
     *
     * @param unitList 单位数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @return 结果
     */
    public String importUnit(List<Unit> unitList, Boolean isUpdateSupport)
    {
        if (StringUtils.isNull(unitList) || unitList.size() == 0)
        {
            return "导入单位数据不能为空";
        }
        int successNum = 0; //  成功条数
        int failureNum = 0; //  失败条数
        StringBuilder successMsg = new StringBuilder(); // 成功信息
        StringBuilder failureMsg = new StringBuilder();  // 失败信息
        for (Unit unit : unitList)
        {
            try
            {
                // 验证是否存在这个单位序号
                Unit info = unitMapper.checkUnitNoUnique(unit.getUnitNo());
                if (StringUtils.isNull(info))
                {
                    unit.setStatus(PublicEnum.NORMAL.getCode()); // 状态：0正常
                    unit = componentTree(unitList, unit);   //  级联处理
                    unitMapper.insert(unit); // 新增
                    successNum++;
                    log.info(successNum + "、单位 [" + unit.getUnitNo() + "]"+ unit.getUnitName() +" 导入成功");
                    //successMsg.append("<br/>" + successNum + "、科目代码 [" + subjectCode.getSubjectCode() + "]"+ subjectCode.getSubjectName() +" 导入成功");
                }
                else if (isUpdateSupport)   // 如果isUpdateSupport=ture，导入数据根据科目代码更新
                {
                    unit.setStatus(PublicEnum.NORMAL.getCode()); // 状态：0正常
                    unit = componentTree(unitList, unit);   //  级联处理
                    unitMapper.updateUnitByNo(unit);    // 更新
                    successNum++;
                    log.info(successNum + "、单位 [" + unit.getUnitNo() + "]"+ unit.getUnitName() + "更新成功");
                    //successMsg.append("<br/>" + successNum + "、科目代码 [" + subjectCode.getSubjectCode() + "]"+ subjectCode.getSubjectName() +" 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append(failureNum + "、单位 [" + unit.getUnitNo() + "]"+ unit.getUnitName() + " 已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = failureNum + "、单位 [" + unit.getUnitNo() + "]"+ unit.getUnitName() + " 导入失败";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：" + failureMsg.toString());
            return failureMsg.toString();
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条");
            return successMsg.toString();
        }
    }

    public Unit componentTree(List<Unit> unitList, Unit info){
        log.info("原值：" + info.getUnitNo());
        // 如果长度为1位，
        if(info.getUnitNo().length()==1){
            info.setParentNo("0");
        }else{
            for(int i=unitList.size()-1; i>=0; i--){
                //for(SubjectCode subjectCode : subjectCodesList){
                // 根据长度截取当前代码
                List<String> list = StringUtils.subStr(info.getUnitNo());
                String code = StringUtils.reverseSubstring(list,unitList.get(i).getUnitNo());
                if(list.size() > 0 && StringUtils.isNotEmpty(code)){
                    log.info("父级返回值：" + code);
                    info.setParentNo(code);
                    break;
                }
            }
        }
        return info;
    }
}
