package com.modules;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@MapperScan({"com.modules.system.dao", "com.modules.project.dao"})
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        log.info("启动开始");
        SpringApplication.run(Application.class, args);
        log.info("启动成功");
    }
}
