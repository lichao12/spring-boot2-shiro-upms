package com.modules.system.controller;

import com.modules.common.utils.IpUtils;
import com.modules.common.utils.ShiroUtils;
import com.modules.common.web.BaseController;
import com.modules.common.web.Result;
import com.modules.system.entity.SysUser;
import com.modules.system.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 登录管理
 *
 * @author lc
 */
@Api(tags = "登录管理")
@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/user")
public class LoginController extends BaseController {

    @Autowired
    private SysUserService userService;

    /**
     * 登录
     *
     * @return
     */
    @ApiOperation(value = "用户登录", notes = "用户登录")
    @GetMapping(value = "/login")
    public Result login(HttpServletRequest request,
                        @ApiParam(name = "loginName", value = "登录账号") @RequestParam(value = "loginName") String loginName,
                        @ApiParam(name = "password", value = "密码") @RequestParam(value = "password") String password) {
        SysUser user = new SysUser();
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(loginName, password);
        try {
            subject.login(token);
            user = ShiroUtils.getUserInfo();
            user.setToken(ShiroUtils.getSession().getId().toString());
        } catch (UnknownAccountException un) {
            log.error("UnknownAccountException=====>未知账户");
            return error("未知账户");
        } catch (IncorrectCredentialsException ic) {
            log.error("IncorrectCredentialsException=====>密码不正确");
            return error("密码不正确");
        } catch (LockedAccountException lo) {
            log.error("LockedAccountException=====>账户已停用");
            return error("账户已停用");
        } catch (ExcessiveAttemptsException ex) {
            log.error("ExcessiveAttemptsException=====>用户名或密码错误次数过多");
            return error("用户名或密码错误次数过多");
        } catch (AuthenticationException au) {
            log.error("AuthenticationException=====>账号或者密码错误");
            return error("账号或者密码错误");
        } finally {
            try {
                user.setLoginDate(new Date());
                user.setLoginIp(IpUtils.getIpAddr(request));
                userService.updateByPrimaryKeySelective(user);
            } catch (Exception e) {
                log.error("IOException=====>更新账户登录信息异常！", e.getMessage());
            }
        }
        return success("登录成功", user);
    }

    /**
     * @Description: 退出登录
     * @author lc
     */
    @ApiOperation(value = "退出登录", notes = "退出登录")
    @GetMapping(value = "/logout")
    public Result logout() {
        ShiroUtils.logout();
        return success("退出成功");
    }

    /**
     * @Description: 未登陆，返回无权限提示
     * @author lc
     */
    @ApiOperation(value = "无权限", notes = "无权限")
    @GetMapping(value = "/loginUnauth")
    public Result loginUnauth() {
        return unauth();
    }
}
