# spring-boot2-shiro-upms

#### 介绍
spring-boot2-shiro-upms是精简的后台管理项目，基于Spring Boot2、Shiro、MyBatis等框架，包含：用户管理、菜单管理、权限管理、登录和权限控制等

#### 技术方案

核心框架：Spring Boot2

ORM框架：Mybatis

安全框架：Shiro

API接口：Swagger2

前端框架：VUE

数据库：MySqL

#### 使用说明

1. 使用IDEA导入本项目
2. 新建数据库;
3. 导入数据库sql/upms.sql
4. 修改(resources/application.yml)配置文件，主要修改数据配置
5. 运行项目
    5.1. 项目根目录下执行mvn springboot:run
    5.2. 直接运行Application.java
6. 浏览器访问http://localhost:8087/umps
7. 访问接口http://localhost:8087/umps/swagger-ui.html

#### 用户密码
账号：admin 密码：admin123

#### 演示图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/154207_03316508_1108042.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/154230_ae8183c1_1108042.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/154251_7d81a591_1108042.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/154307_41441fc4_1108042.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/154325_b5078ed1_1108042.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/154341_903e2f77_1108042.png "屏幕截图.png")

